function PermissionFactory(constructor, name) {
    Object.defineProperty(constructor.prototype, 'permissions', {
			get: function() {
				const permissions = this.$store.getters['user/getPermissions'];

                if (name) {
                    return permissions[name];
                }

                return permissions;
			}
		});
}

function Permission(name = '') {
    if (typeof name === 'string') {
        return (constructor) =>  PermissionFactory(constructor, name);
    }

	return PermissionFactory(name);
}

export default Permission;
